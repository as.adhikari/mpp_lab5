package lab5.qn4;

import java.time.LocalDate;

public class CustomerOrderFactory {

    private static Customer cust;

    CustomerOrderFactory() {
    }

    public static void createCustOrder(String custName, String[] items) {
        cust = new Customer(custName);
        Order order = new Order(LocalDate.now());
        for (String s : items) {
            order.addItem(s);
        }
        cust.addOrder(order);
    }

    public static void addNewOrder(String[] items) {
        Order order = new Order(LocalDate.now());
        for (String s : items) {
            order.addItem(s);
        }
        cust.addOrder(order);
    }

    public static void displayOrders() {
        System.out.println(cust.getOrders());
    }
}
