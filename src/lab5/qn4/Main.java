package lab5.qn4;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        Customer cust = new Customer("Bob");
        Order order = Order.newOrder(cust, LocalDate.now());
        order.addItem("Shirt");
        order.addItem("Laptop");

        order = Order.newOrder(cust, LocalDate.now());
        order.addItem("Pants");
        order.addItem("Knife set");

        System.out.println(cust.getOrders());

        //Construction of Order object is controlled by Factory method
        CustomerOrderFactory.createCustOrder("Bob", new String[]{"Shirt", "Laptop"});
        CustomerOrderFactory.addNewOrder(new String[]{"Pants", "Knife set"});
        System.out.println("using Factory class");
        CustomerOrderFactory.displayOrders();
    }
}
