package lab5.qn3;

public class Bus implements Vehicle {

    @Override
    public void startEngine() {
        System.out.println("Bus is starting");
    }
}
