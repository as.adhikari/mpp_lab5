package lab5.qn3;

public class Main {

    public static void main(String[] args){

        String[] vehicles = { "Bus", "Car", "Bike", "Electric Car", "Truck" };
        for (String s : vehicles)
            VehicleFactory.getVehicle(s);

    }
}
