package lab5.qn3;

public class Truck implements Vehicle {

    @Override
    public void startEngine() {
        System.out.println("Truck is starting");
    }
}
