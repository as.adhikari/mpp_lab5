package lab5.qn3;

public class ElectricCar implements Vehicle {

    @Override
    public void startEngine() {
        System.out.println("Electric Car is starting");
    }
}
