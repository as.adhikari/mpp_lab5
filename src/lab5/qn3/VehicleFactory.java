package lab5.qn3;

public class VehicleFactory {

    private VehicleFactory(){

    }

    public static void getVehicle(String vehicle) {
        Vehicle v;
        switch (vehicle) {
            case "Bus":
                v = new Bus();
                v.startEngine();
                break;
            case "Truck":
                v = new Truck();
                v.startEngine();
                break;
            case "Car":
                v = new Car();
                v.startEngine();
                break;
            case "Electric Car":
                v = new ElectricCar();
                v.startEngine();
                break;
            default:
                System.out.println("No vehicle found");
        }
    }

}
