package lab5.qn2;

public final class Circle implements Figure {

    private final double radius;

    Circle(double radius){
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public double computeArea() {
        return Math.PI*radius*radius;
    }
}
