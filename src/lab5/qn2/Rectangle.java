package lab5.qn2;

public final class Rectangle implements Figure {

    private final double height;
    private final double width;

    Rectangle(double height, double width){
        this.height = height;
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    @Override
    public double computeArea() {
        return height*width;
    }
}
