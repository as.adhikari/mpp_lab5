package lab5.qn2;

public interface Figure {
    public double computeArea();
}
