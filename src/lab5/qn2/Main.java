package lab5.qn2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args){
        Figure figure1 = new Triangle(2,1.5);
        Figure figure2 = new Circle(1.4);
        Figure figure3 = new Circle(2);
        Figure figure4 = new Triangle(2,1.1);
        Figure figure5 = new Circle(1.2);
        Figure figure6 = new Rectangle(2,1);
        Figure figure7 = new Rectangle(3,2);
        Figure figure8 = new Triangle(2,1.6);
        Figure figure9 = new Rectangle(1,2);
        Figure[] figureArray = {figure1,figure2,figure3,figure4,figure5,figure6,figure7,figure8,figure9};

        List<Figure> figures = new ArrayList<>(Arrays.asList(figureArray));

        double totalAreas = 0;
        for(Figure figure:figures){
            totalAreas += figure.computeArea();
        }

        System.out.println("Total Area: "+totalAreas);
    }
}
