package lab5.qn1;

public class MuteQuack implements IQuackBehaviour {

    @Override
    public void quack() {
        System.out.println("Cannot Quack.");
    }
}
