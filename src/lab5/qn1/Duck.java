package lab5.qn1;

public abstract class Duck {

    private IFlyBehaviour flyBehaviour;
    private IQuackBehaviour quackBehaviour;

    Duck(IFlyBehaviour flyBehaviour, IQuackBehaviour quackBehaviour){
        this.flyBehaviour = flyBehaviour;
        this.quackBehaviour = quackBehaviour;
    }
    public void swim(){
        System.out.println("Swimming");
    }

    public void fly(){
        flyBehaviour.fly();
    }

    public void quack(){
        quackBehaviour.quack();
    }

    public abstract void display();
}
