package lab5.qn1;

public class MallardDuck extends Duck{

    MallardDuck(){
        super(new FlyWithWings(), new Quack());
    }

    @Override
    public void display() {
        System.out.println("display");
    }
}
