package lab5.qn1;

public class CannotFly implements IFlyBehaviour {

    @Override
    public void fly() {
        System.out.println("Can not Fly with Wings.");
    }
}
