package lab5.qn1;

public class Squeak implements IQuackBehaviour {
    @Override
    public void quack() {
        System.out.println("squeaking");
    }
}
