package lab5.qn1;

public class RedHeadDuck extends Duck{

    RedHeadDuck(){
        super(new FlyWithWings(), new Quack());
    }

    @Override
    public void display() {
        System.out.println("displaying");
    }
}
