package lab5.qn1;

public class RubberDuck extends Duck {

    RubberDuck(){
        super(new CannotFly(),new Squeak());
    }

    @Override
    public void display() {
        System.out.println("displaying");
    }
}
